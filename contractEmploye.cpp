#include <ineio/ineio.hpp>
#include <string>

using namespace ineio;
using namespace std;

class [[ineio::contract("employee")]] employee : public contract{
    public:
    using ineio::contract::contract;
    employee(name reciever, name code, datastream<const char*>ds) : contract(reciever, code, ds), 
    _tblEmployee(get_self(), get_self().value), _tblPerson(get_self(), get_self().value),
    _tblAddress(get_self(), get_self().value), _tblContact(get_self(), get_self().value), _tblProfile(get_self(), get_self().value) {}

    [[ineio::action]] void craddress(string state, string city, string street, string number)
    {
        _tblAddress.emplace(get_self(), [&](auto row){
            row.id = _tblAddress.available_primary_key();
            row.state = state;
            row.city = city;
            row.number = number;
        });
    }
    [[ineio::action]] void crcontact(string name, string surname, string number, string email)
    {
        _tblContact.emplace(get_self(), [&](auto row){
            row.id = _tblContact.available_primary_key();
            row.name = name;
            row.surname = surname;
            row.number = number;            
        });
    }
    [[ineio::action]] void crprofile(int age, string gender, string title)
    {
        _tblProfile.emplace(get_self(), [&](auto row){
            row.id = _tblProfile.available_primary_key();
            row.name = name;
            row.surname = surname;
            row.number = number;            
        });
    }
    [[ineio::action]] void crperson(uint64_t ContactId, uint64_t AddressId, uint64_t ProfileId)
    {
        
        _tblPerson.emplace(get_self(), [&](auto row){
            row.id = _tblPerson.available_primary_key();
            row.Contactid = ContactId;
            row.Addressid = AddressId;
            row.Profileid = ProfileId;      
        });
    }
    [[ineio::action]] void crempolyee(uint64_t personId, string company, string position )
    {
        _tblEmployee.emplace(get_self(), [&](auto row){
            row.id = _tblEmployee.available_primary_key();
            row.personId = personId;
            row.company = company;
            row.position = position;
        });
    }
    [[ineio::action]] void readrange(uint64_t low, uint64_t high){
        if(low > high){
            temp = low;
            low = high;
            high = temp;
        }
        do {
            auto itr = _tbl.find(low);
            print("Name: ", itr->owner, "| ");
            print("User ID: ", itr->id, "| ");
            print("Email: ", itr->email), "| ";
            print("Phone: ", itr->number), "| ";
            low++;
        }while(low<high)
    }

    protected:
    struct [[ineio::table]] employe {
        uint64_t    id;
        uint64_t    personid;
        string      company;
        string      position;

        uint64_t primary_key() const {return id;}
    };
    struct[[ineio::table]] person {
        uint64_t    id;
        uint64_t    Contactid;
        uint64_t    Addressid;
        uint64_t    Profileid;

        uint64_t primary_key() const {return id;}
    };
    struct [[ineio::table]] address {
        uint64_t    id;
        string      state;
        string      city;
        string      number;

        uint64_t primary_key() const {return id;}
    };
    struct [[ineio::table]] contact {
        string  name;
        string  surname;
        string  number;
        string  email;

        uint64_t primary_key() const {return id;}
    };
    struct [[ineio::table]] profile {
        uint64_t id;
        int age;
        string  gender;
        string  title;
        uint64_t primary_key() const {return id;}

    }
    typedef ineio::multi_index<"addresstbl"_n, address> addr_tbl;
    typedef ineio::multi_index<"persontbl"_n, person> prsn_tbl;
    typedef ineio::multi_index<"contacttbl"_n, contact> cnct_tbl;
    typedef ineio::multi_index<"employeetbl"_n, employe> empl_tbl;
    typedef ineio::multi_index<"profiletbl"_n, profile> prfl_tbl;

    empl_tbl _tblEmployee;
    cnct_tbl _tblContract;
    prsn_tbl _tblPerson;
    addr_tbl _tblAddress;
    prfl_tbl _tblProfile;
    

};

