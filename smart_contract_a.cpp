#include <ineio/ineio.hpp>
#include <string>

using namespace ineio;
using namespace std;

class [[ineio::contract("prototype")]] prototype : public contract{
    public:
    using ineio::contract::contract;
    prototype(name reciever, name code, datastream<const char*>ds) : contract(reciever, code, ds), 
    _tbl(get_self(), get_self().value) {}

    [[ineio::action]] void create(ineio::name owner, std::string email, int number)
    {
        auto itr = _tbl.find(owner.value);
        check(itr == _tbl.end(), "User with that name already exist, please use difrent name");

        _tbl.emplace(get_self(), [&](auto row){
            row.id = _tbl.available_primary_key();
            row.owner = owner;
            row.email = email;
            row.number = number;
        });
    }
    [[ineio::action]] void readbyid(uint64_t id)
    {
        auto itr = _tbl.find(id);
        check(itr != _tbl.end(), "User with that id doesn't exist, please select valid id");

        print("Name : ", itr->owner, "| ");
        print("ID : ", itr->id, "| ");
        print("E-mail : ", itr->email, "| ");
        print("Phone : ", itr->number, "| ");

    }
    [[ineio::action]] void readbyname(ineio::name owner)
    {
        auto itr = _tbl.find(owner.value);

        print("Name : ", itr->owner, "| ");
        print("ID : ", itr->id, "| ");
        print("E-mail : ", itr->email, "| ");
        print("Phone : ", itr->number, "| ");
    }
    [[ineio::action]] void removebyid(uint64_t id)
    {
        auto itr = _tbl.find(id);
        check(itr != _tbl.end(), "User with that id doesn't exist, please select valid id");

        _tbl.erase(itr);
    }
    [[ineio::action]] void update(uint64_t id, string email, int number)
    {
        auto itr = _tbl.find(id);
        check(itr != _tbl.end(), "User with that id doesn't exist, please select valid id");

        _tbl.modify(itr, get_self(), [&](auto &row){
            row.email = email;
            row.number = number;
        });
    }
    [[ineio::action]] void count(){
        int count = 0;
        for(auto itr : _tbl){
            count++;
        }
        print("Number of users: ", count);
    }
    [[ineio::action]] void removebymail(std::string mail){
        
        std::vector<uint64_t> keysForDeletion;

        for(auto& itr : _tbl) {
            if (itr.email == mail) {
                keysForDeletion.push_back(itr.id);   
            }
        }
        for (uint64_t id : keysForDeletion) {
            print("remove from persons ", id);
            auto itr = _tbl.find(id);
            if (itr != _tbl.end()) {
                _tbl.erase(itr);
            }
        }
    }
    [[ineio::action]] void readrange(uint64_t low, uint64_t high){
        if(low > high){
            temp = low;
            low = high;
            high = temp;
        }
        do {
            auto itr = _tbl.find(low);
            print("Name: ", itr->owner, "| ");
            print("User ID: ", itr->id), "| ";
            print("Email: ", itr->email), "| ";
            print("Phone: ", itr->number), "| ";
            low++;
        }while(low<high)
    }
    [[ineio::action]] void readall(){
        for(auto itr : _tbl){
            print("Name : ", itr->owner, "| ");
            print("User ID: ", itr->id,  "| ");
            print("Email : ", itr->email, "| ");
            print("Phone : ", itr->number, "| ");
        }
    }
    [[ineio::action]] void removeall (){
        std::vector<uint64_t> keysForDeletion;

        for(auto& itr : _tbl) {
                keysForDeletion.push_back(itr.id);   
        }
        for (uint64_t id : keysForDeletion) {
            auto itr = _tbl.find(id);
            if (itr != _tbl.end()) {
                _tbl.erase(itr);
            }
        }
    }
    void printUsr()
    private:
    struct [[ineio::table]] person {
        uint64_t    id;
        ineio::name owner;
        string      email;
        int         number;

        uint64_t primary_key() const {return id;}
        uint64_t name_key() const {return owner.value;}
    };
    typedef ineio::multi_index<"person"_n, person, ineio::indexed_by<"owner"_n, ineio::const_mem_fun<person, uint64_t, &person::name_key >>
    >   person_table;

    person_table _tbl;

};

